function Copy-Arma3-Keys {
    param (
        [string]    $modset = $defaultModset
    )

    $currentKeys = Get-ChildItem -Path $armaKeysDir\!* -Include *.txt
    $targetKeys = Get-ChildItem -Path $configKeysDir\!* -Include *.txt
    $desiredtxt = "!$modset.txt"
    # Check if there are correct bikeys for selected modset loaded in keys folder
    if (($currentKeys | Select-Object  -ExpandProperty Name) -ne $desiredtxt) {
        Write-Host "Incorrect bikeys found. Found $($currentKeys.Name) instead of $desiredtxt. Preparing to replace."
        $confirmation = TimedPrompt "Do You wish to replace bikeys? Press [n] to skip or any key to replace: " 5
        if ($confirmation -eq 'N') {
            Write-Host "Bikey replacement ommited."
        } else {
            Write-Host "Removing incorrect bikeys from $armaKeysDir"
            Remove-Item "$armaKeysDir\*.*"
            Write-Host "Copying correct bikeys from $configKeysDir to keys"
            Copy-Item "$configKeysDir\*" -Destination "$armaKeysDir"
        }
    # Or if there are correct but outdated bikeys
    } elseif ($currentKeys.LastWriteTime -lt $targetKeys.LastWriteTime) {
        Write-Host "There was $modset bikey update at $($targetKeys.LastWriteTime), old bikeys are from $($currentKeys.LastWriteTime)."
        $confirmation = TimedPrompt "Do You wish to replace bikeys? Press [n] to skip or any key to update: " 5
        if($confirmation -eq 'N') {
            Write-Host "Bikey update ommited."
        } else {
            Write-Host "Removing old bikeys from $armaKeysDir"
            Remove-Item "$armaKeysDir\*.*"
            Write-Host "Copying updated bikeys from $configKeysDir to $armaKeysDir"
            Copy-Item "$configKeysDir\*" -Destination "$armaKeysDir"
        }
    } else {
        Write-Host "Correct bikeys found."
    }
}
