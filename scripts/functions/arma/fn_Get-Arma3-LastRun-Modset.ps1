function Get-Arma3-LastRun-Modset {
    try {
        $txt = ((Get-ChildItem -Path "$armaKeysDir\*" -Include *.txt) | Where-Object {$_.Name[0] -eq '!'} | Select-Object -ExpandProperty Name)
        # modset file format "!name.txt"
        $modset = $txt.Substring(1,$txt.length-5)
    } catch {
        return $defaultModset
    }

    return $modset
}