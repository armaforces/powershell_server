function Get-Arma3-MissingMods {
    param (
        [array]     $modsArray
    )

    $missingMods = Create-List
    foreach ($mod in $modsArray) {
        $test = Test-Path -Path $mod.path
        if (($test -eq $false) -and ($mod.local_mod -ne $true)) {
            $missingMods.Add($mod)
        } elseif ($test -eq $false) {
            Write-Host 'Local mod: '$mod.name' is missing' -BackgroundColor Red
        }
    }

    return ,$missingMods
}
