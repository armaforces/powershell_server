function Get-Arma3-StartCommand {
    param (
        [string]    $armaDir,
        [array]     $modsServer,
        [array]     $modsNormal,
        [string]    $modset,
        [string]    $port,
        [string]    $headless,
        [string]    $armaServer = "arma3server_x64.exe"
    )

    if ($headless -eq $false) {
        # Params for dedicated server
        $params = @(
            "-port=$port"
            """-config=$configServerPath"""
            """-cfg=$configBasicPath"""
            "-profiles=""$configProfileDir\server"""
            "-name=server"
            "-filePatching -netlog"
            "-limitFPS=100"
            "-loadMissionToMemory"
        ) -join ' '
        $mods = @(
            """-serverMod=$($modsServer.path -join ';')"""
            """-mod=$($modsNormal.path -join ';')"""
        ) -join ' '
    }
    else {
        # Params for headless client
        $params = @(
            "-client -connect=127.0.0.1"
            "-port=$port"
            "-password=$serverPassword"
            "-profiles=""$configProfileDir\HC"""
            "-limitFPS=100"
        ) -join ' '
        $modsHC = $modsNormal + $modsServer
        $mods = """-mod=$($modsHC.path -join ';')"""
    }

    return @(
        """$armaDir\$armaServer"""
        $params,
        $mods,
        "-enableHT"
    )
}
