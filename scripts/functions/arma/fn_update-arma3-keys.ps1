function Update-Arma3-Keys {
    param (
        [string]    $modset = $defaultModset,
        [array]     $modsArray
    )

    New-Item -ItemType Directory -Force "$configKeysDir\"

    $ModsetBikeys = Create-List
    $UpdateBikeys = Create-List
    $NoBikeys = Create-List
    # Check all modset mods directories for bikeys
    # And check if they are newer than their counterparts in keys_modset (or there are none)
    foreach ($mod in $modsArray) {
        if ((Test-Path -Path $mod.path)) {
            $bikeys = Get-ChildItem -Path ($mod.path + "\*") -Include *.bikey -Recurse
        } else {
            Write-Host ($mod.name + " folder not found!") -BackgroundColor Red
            $bikeys = @()
        }

        if ($bikeys.count -eq 0) {
            Write-Host ($mod.name + " has no bikey!") -BackgroundColor Red
            $NoBikeys.Add($mod)
        }
        foreach ($bikey in $bikeys) {
            $ModsetBikeys.Add($bikey)
            $bikeyName = $bikey | Select-Object -Expand Name
            $bikeyDate = $bikey | Select-Object -Expand LastWriteTime
            $test = Test-Path -Path "$configKeysDir\$bikeyName"
            if ($test -eq $true) {
                $oldBikey = Get-Item "$configKeysDir\$bikeyName"
                $oldBikeyDate = $oldBikey | Select-Object -Expand LastWriteTime
                if ($oldBikey.LastWriteTime -lt $bikey.LastWriteTime) {
                    Write-Host ($mod.name + " has newer bikey available.")
                    $UpdateBikeys.Add($bikey)
                }
            } else {
                Write-Host ($mod.name + " has new bikey available.")
                $UpdateBikeys.Add($bikey)
            }

        }
    }
    $NoBikeysCount = $NoBikeys.count
    # Add Arma 3 default bikey check
    $ModsetBikeys.Add((Get-Item $armaBikey))
    if (-not (Test-Path -Path "$configKeysDir\$($armaBikey.split('\')[-1])")) {
        Write-Host ("Arma 3 has newer bikey available.")
        $UpdateBikeys.Add($armaBikey)
        $NoBikeysCount += 1
    }
    $ModsetBikeys.Add($armaBikey)
    if ($NoBikeysCount -gt 0) {
        Write-Host "$NoBikeysCount mods don't have bikeys!" -BackgroundColor Red
    }

    # Get list of keys in keys_modset
    $DirectoryBikeys = Get-ChildItem -Path "$configKeysDir\*" -Include *.bikey
    # Select bikeys 
    $UnnecessaryBikeys = $DirectoryBikeys | Where-Object {$_.name -notin ($modsetBikeys | ForEach-Object {$_.Name})}

    # Handle updating keys_modset directory
    $NewBikeysCount = $UpdateBikeys.count
    if ($NewBikeysCount -gt 0 -or $UnnecessaryBikeys.count -gt 0) {
        # Handling keys_modset in directory but not in modset
        if ($UnnecessaryBikeys.count -gt 0) {
            foreach ($unnecessaryBikey in $UnnecessaryBikeys) {
                Write-Host "Wrong $($unnecessaryBikey.Name) in $modset keys folder." -BackgroundColor Red
            }
        }
        $confirmation = TimedPrompt "There are $NewBikeysCount new bikeys available and $($UnnecessaryBikeys.count) unnecessary/wrong bikeys. Do you want to update $modset bikeys? Press [n] to skip or any key to update: " 3
        if ($confirmation -eq 'N') {
            Write-Host "$modset bikey update cancelled." -BackgroundColor DarkYellow
        } else {
            Write-Host "Copying $NewBikeysCount new bikeys to $configKeysDir\"
            foreach ($bikey in $UpdateBikeys) {
                Copy-Item $bikey -Destination "$configKeysDir\" -Force
            }
            Write-Host "Removing $($UnnecessaryBikeys.count) unnecessary bikeys from $configKeysDir\"
            foreach ($bikey in $UnnecessaryBikeys) {
                Remove-Item -Path $bikey -Force
            }
            # Update !modlist.txt file
            (Get-Date | Out-String).Trim() | Out-File -FilePath "$configKeysDir\!$modset.txt" -Force -Encoding utf8
            ($modset |Out-String).Trim() | Out-File -FilePath "$configKeysDir\#current-modset.txt" -Force -Encoding utf8
        }
    } else {
        Write-Host "No new bikeys found. All keys are up to date." -BackgroundColor Green
    }
}
