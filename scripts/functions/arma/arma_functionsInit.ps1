#################################
#
#             ARMA
#
#################################

# Function to check if all mods are present on server
. "$PSScriptRoot\fn_check-arma3-mods.ps1"

# Function to check if server is already running
. "$PSScriptRoot\fn_Check-Arma3-ServerRunning.ps1"

# Function copies correct bikeys to keys folder
. "$PSScriptRoot\fn_copy-arma3-keys.ps1"

# Function returns list of missing mods
. "$PSScriptRoot\fn_get-arma3-missingMods.ps1"

# Function returns modset that server was run with last time
. "$PSScriptRoot\fn_Get-Arma3-LastRun-Modset.ps1"

# Function returns mods array
. "$PSScriptRoot\fn_get-arma3-modsArray.ps1"

# Function returns server start command
. "$PSScriptRoot\fn_get-arma3-startCommand.ps1"

# Function handles server startup process
. "$PSScriptRoot\fn_run-arma3-server.ps1"

# Function handles changing server startup params
. "$PSScriptRoot\fn_Set-Arma3-StartupParams.ps1"

# Function handles bikey check and update
. "$PSScriptRoot\fn_update-arma3-keys.ps1"