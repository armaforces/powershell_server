function Get-Arma3-ModsArray {
    param (
        [string]    $modset
    )

    try {
        $modsImport = import-csv "$modsetsDir\$modset.csv" -Delimiter ';'
    } catch [System.Management.Automation.ItemNotFoundException] {
        return $null
    }
    # Add path property to mods
    $modsImport | ForEach-Object {
        if ($_.id -ne "local") {
            $_ | Add-Member path ("$modsDir\"+ $_.id)
        } else {
            $_ | Add-Member path ("$armaDir\"+ $_.name)
            $_ | Add-Member local_mod $true
        }
    }

    $modsServer = ($modsImport | Where-Object {$_.'is_serverside' -eq 'True' -or $_.'is_map' -eq 'True'})
    $modsNormal = $modsImport | Where-Object {$_.'is_serverside' -ne 'True' -and $_.'is_map' -ne 'True' -and $_.'is_optional' -ne 'True'}
    $modsOptional = $modsImport | Where-Object {$_.'is_optional' -eq 'True'}

    $modsHashTable = @{
        'serverMods' = $modsServer;
        'normalMods' = $modsNormal;
        'optionalMods' = $modsOptional
    }
    return $modsHashTable
}
