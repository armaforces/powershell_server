function Run-Arma3-Server {
    param (
        [string]    $modset,
        [string]    $armaDir,
        [string]    $port,
        [boolean]   $headless
    )

    Check-Arma3-ServerRunning
    $serverType = if($headless -eq $false) {'Server'} else {'Headless Client'}
    Write-SectionSeparation "Starting $modset $serverType on port $port."
    # Get mods list
    $mods = Get-Arma3-ModsArray $modset
    $modsServer = $mods['serverMods']
    $modsNormal = $mods['normalMods']
    $modsOptional = $mods['optionalMods']
    $modsArray = @()
    $modsArray += $modsServer
    $modsArray += $modsNormal
    $modsArray += $modsOptional
    # Check if mods are correctly loaded
    if ($null -eq $mods) {
        Throw "No $modset preset available."
    } else {
        Write-Host "Successfully loaded $modset mod preset." -BackgroundColor Green
    }
    # Check if all mods are available on server
    $modsArray = Check-Arma3-Mods $modsArray
    # Check if all mods are up-to-date
    Write-SectionSeparation "Checking if mods are up-to-date."
    $updateRequired = Check-Mod-Update $modsArray
    # Get mods size
    $modsSize = get-modsSize $modsArray
    $modsCount = ($modsArray | Where-Object {$_.is_optional -ne 'True'} ).count
    if ($modsCount -gt 0) {
        Write-Host "Loading $modsCount mods. Total mods size = $modsSize."
    } else {
        Write-Host "No mods specified. Launching clean server."
    }
    # If dedicated server then update bikey
    if ($headless -eq $false) {
        # Update bikeys
        Write-SectionSeparation "Updating $modset bikeys from mod folders."
        Update-Arma3-Keys $modset $modsArray
        # Copy corresponding keys
        Write-SectionSeparation "Checking keys folder if there are correct bikeys."
        Copy-Arma3-Keys $modset #Add check for !.txt if has newer last update time
        Write-SectionSeparation "Preparing server configuration files."
        Load-ServerConfig $modset
    }
    # Get launch command from params
    $launchCommand = Get-Arma3-StartCommand $armaDir $modsServer $modsNormal $modset $port $headless
    # Start server
    Write-SectionSeparation "Launching server." -BackgroundColor Green
    Start-Process $launchCommand[0] -ArgumentList ($launchCommand | Select-Object -Skip 1)
    WaitUntil-ServerStarted
    Write-Host "Console termination in 60 seconds."
    Start-Sleep -s 60
}
