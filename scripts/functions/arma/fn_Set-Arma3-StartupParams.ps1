function Set-Arma3-StartupParams {
    param (
        [string]    $modset,
        [bool]      $headless,
        [int]       $port,
        [string]    $serverType
    )
    

    # Call function handling modset selection
    $modset = Select-Modset $modset

    # Handle headless selection
    $headlessInput = Get-Input "Do you want to run regular [S]erver or [H]eadless Client? [Enter to $serverType]: "
    if ($headlessInput -ne "") {
        if ($headlessInput -eq 'H') {
            $headless = $true
        } else {
            $headless = $false
        }
    }
    $serverType = if ($headless -eq $false) {'Server'} else {'Headless Client'}
    Write-Host "Selected $serverType!"

    # Handle port selection
    $portInput = Get-Input "Which port do you want to run server on? [Enter to $port]: "
    if ($portInput -ne "") {
        $port = $portInput
    }
    Write-Host "Selected $port port!"

    # Build return HashTable
    $paramsHashTable = @{
        'modset'    = $modset;
        'headless'  = $headless;
        'port'      = $port
    }

    return $paramsHashTable
}