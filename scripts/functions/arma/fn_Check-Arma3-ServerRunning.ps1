function Check-Arma3-ServerRunning {
    param (
        [bool]  $headless
    )

    if ($headless -eq $false) {
        if (Get-Process | Where-Object {$_.MainWindowTitle -Like "Arma 3 Console *"} -ErrorAction SilentlyContinue) {
            # Abort if server is already running and user did not confirm
            Write-Host "Server is already running, are you sure you want to proceed?!" -ForegroundColor Red
            $confirmation = TimedPrompt "Press [Y] to run confirm: " 60
            if ($confirmation -ne 'Y') {
                Throw 'Server is already running and no user confirmation, aborting!'
            }
        }
    }
    
}