function Check-Arma3-Mods {
    param (
        [System.Collections.ArrayList]     $modsArray
    )

    $missingMods = Get-Arma3-MissingMods $modsArray
    $missingModsCount = $missingMods.count
    if ($missingModsCount -gt 0) {
        Write-Host "Found $missingModsCount missing mods:" -BackgroundColor Red
        foreach ($mod in $missingMods) {
            Write-Host $mod.name -BackgroundColor Red
        }
        $download = Get-Input "Do you want to download missing mods? [Y/N]"
        if ($download -eq 'Y') {
            get-mod-update $missingMods
        } else {
            $continue = Get-Input "Do you want to start server anyway? [Y/N]: "
            if ($continue -eq 'Y') {
                Write-Host "Ignoring missing mods."
                foreach ($mod in $missingMods) {
                    $modsArray.Remove($mod)
                }
            } else {
                Write-Host "Terminating startup."
                Start-Sleep -s 1
                break
            }
        }
    } else {
        Write-Host "All mods are available." -BackgroundColor Green
    }
    return $modsArray
}