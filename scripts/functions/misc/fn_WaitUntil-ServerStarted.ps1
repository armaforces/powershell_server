function WaitUntil-ServerStarted {

    $runningServers = Get-Process | Where-Object {$_.MainWindowTitle -Like "Arma 3 Console *"}
    $serverStartupTime = [system.diagnostics.stopwatch]::startNew()

    while ($runningServers -eq (Get-Process | Where-Object {$_.MainWindowTitle -Like "Arma 3 Console *"}) -or $serverStartupTime.Elapsed.TotalSeconds -gt 120) {
        Start-Sleep 1
    }

    if ($serverStartupTime.Elapsed.TotalSeconds -gt 120) {
        Throw "There is a problem with server startup!"
    } else {
        if ($headless -ne $true) {
            Write-Host "Server successfully started in $([math]::Round($serverStartupTime.Elapsed.TotalSeconds,2)) seconds." -BackgroundColor Green
        }
    }
}