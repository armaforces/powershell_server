Function TimedPrompt($prompt,$secondsToWait){

    if ($null -ne $psISE) {
        Write-Host "ISE detected, falling back to Read-Host!" -NoNewline -ForegroundColor Yellow
        return Read-Host $prompt
    }

    Write-Host -NoNewline $prompt -BackgroundColor DarkBlue
    $secondsCounter = 0
    $subCounter = 0
    While ( (!$host.ui.rawui.KeyAvailable) -and ($count -lt $secondsToWait) ){
        start-sleep -m 10
        $subCounter = $subCounter + 10
        if($subCounter -eq 1000)
        {
            $secondsCounter++
            $subCounter = 0
            Write-Host -NoNewline "."
        }
        If ($secondsCounter -eq $secondsToWait) {
            Write-Host "`r"
            return $false;
        }
    }
    Write-Host "`r"
    return ([string]($host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown").Character)).ToUpper();
}
