function Get-Input {
    param (
        [string]    $prompt
    )

    Write-Host $prompt -BackgroundColor DarkBlue
    $read = Read-Host "> "
    try {
        $read = [int]$read
    } catch {
        if ($read.Length -eq 1) {
            $read = $read.ToUpper()
        }
    }
    return $read
}