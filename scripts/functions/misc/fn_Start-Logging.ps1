function Start-Logging {
    param (
        [string] $logsDir,
        [string] $logFileName,
        [string] $logFileExtension = "log"
    )

    $timestamp = Get-Date -Format o | ForEach-Object { $_ -replace ":", "." }
    $logFilePath = "$logsDir\$logFileName-$timestamp.$logFileExtension"

    $lastMonth = (Get-Date).AddMonths(-1)
    Get-ChildItem "$PSScriptRoot" | Where-Object {$_.LastAccessTime -lt $lastMonth} | Remove-Item

    Start-Transcript $logFilePath
}
