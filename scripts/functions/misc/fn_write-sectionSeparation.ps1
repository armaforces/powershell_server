function Write-SectionSeparation {
    param (
        [string]    $sectionName
    )

    Write-Host "`r"
    Write-Host "*********************************************"
    Write-Host "**** $sectionName"
    Write-Host "*********************************************"
    Write-Host "`r"
}