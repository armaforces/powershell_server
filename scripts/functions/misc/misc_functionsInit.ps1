#################################
#
#             MISC
#
#################################

# Function to create list
. "$PSScriptRoot\fn_create-list.ps1"

# Converts Epoch time into standard date
. "$PSScriptRoot\fn_get-DateFromEpoch.ps1"

# Prompts user for input
. "$PSScriptRoot\fn_get-Input.ps1"

# Function waits until server is started and counts startup time
. "$PSScriptRoot\fn_WaitUntil-ServerStarted.ps1"

# Function writes section separation
. "$PSScriptRoot\fn_write-sectionSeparation.ps1"

# Function initializes loggin to file
. "$PSScriptRoot\fn_Start-Logging.ps1"

# Function stops logging to file
. "$PSScriptRoot\fn_Stop-Logging.ps1"

# Function handles showing timed prompts
. "$PSScriptRoot\fn_timedPrompt.ps1"
