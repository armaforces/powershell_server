function get-DateFromEpoch ($epochDate) {
    return [timezone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($epochDate))
}