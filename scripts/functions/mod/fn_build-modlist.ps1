function build-modlist {
    param (
        [array]     $modsInfo
    )

    $mods = Create-List
    $specials = import-special-modInfo
    foreach ($modInfo in $modsInfo) {
        $serverSide = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_serverside -contains 'True'
        $map = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_map -contains 'True'
        $optional = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_optional -contains 'True'

        $mod = New-Object PSObject -Property @{
            id              = $modInfo.publishedfileid
            name            = $modInfo.title
            is_serverside   = $serverSide
            is_map          = $map
            is_optional     = $optional
            size            = $modInfo.file_size
        }
        $mods += $mod
    }

    # Add optional and server mods
    foreach ($mod in $specials) {
        $optional = $mod.is_optional -eq 'True'
        $serverSide = $mod.is_serverside -eq 'True'
        if ($optional -or $serverSide -and $mods.name -notcontains $mod.name) {
            $mods += $mod
        }
    }

    # Count mods
    $countMods = $mods.count
    $countOptional = ($mods | Where-Object {$_.'is_optional' -eq 'True'}).count
    $countServer = ($mods | Where-Object {$_.'is_serverside' -eq 'True'}).count
    $countMap = ($mods | Where-Object {$_.'is_map' -eq 'True'}).count
    Write-Host "Mod list ready with $countMods mods:"
    Write-Host "- $countOptional optional"
    Write-Host "- $countServer server"
    Write-Host "- $countMap maps"

    return $mods
}