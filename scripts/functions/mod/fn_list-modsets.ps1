function List-Modsets {

    param (
        [array] $availableModsets = @()
    )
    
    Write-Host "Available modsets:`n"
    if ($availableModsets.Count -eq 0) {
        $availableModsets = Get-ModsetsList
    }
    ($availableModsets | Format-Table -Property 'No.', 'Modset name', 'Mod quantity', LastWriteTime | Out-String).Trim() | Write-Host
    Write-Host ""
}