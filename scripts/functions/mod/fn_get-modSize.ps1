function get-modSize {
    param (
        [string]    $modID
    )

    $directory = "$modsDir\$modID"
    $files = $directory | Get-ChildItem -Force -Recurse | Where-Object {-not $_.PSIsContainer}
    if ($files) {
        return ($files | Measure-Object -Sum -Property Length).Sum
    } else {
        return 0
    }
}