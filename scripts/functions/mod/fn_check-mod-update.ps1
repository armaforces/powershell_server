function Check-Mod-Update {

    param (
        [array]     $mods
    )

    if ($mods.count -eq 0) {
        $mods = create-modInfo-list $modsDir
    } else {
        $modsInfo = steamAPI-get-modObject ($mods | Where-Object {$_.id -ne 'local'} | Select-Object -ExpandProperty id)
    }
    $modsServer = import-server-modInfo

    $updateRequired = Create-List
    foreach ($mod in $modsInfo) {
        $old = ($modsServer | Where-Object {$_.'id' -eq $mod.id} ).last_update_ws -lt $mod.last_update_ws
        if ($old) {
            $updateRequired += $mod
        }
    }

    if ($updateRequired.count -gt 0) {
        $updateRequiredCount = $updateRequired.count
        Write-Host "Found $updateRequiredCount outdated mods:" -BackgroundColor Red
        foreach ($mod in $updateRequired) {
            Write-Host $mod.name -BackgroundColor Red
        }
        $download = Get-Input "Do you want to update mods? [Y/N]"
        if ($download -eq 'Y') {
            get-mod-update $updateRequired
        } else {
            Write-Host "Skipping mod update." -BackgroundColor Yellow
        }
    } else {
        Write-Host "Mods are up-to-date!" -BackgroundColor Green
    }
    return $updateRequired
}