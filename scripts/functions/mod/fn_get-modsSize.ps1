function get-modsSize {
    param (
        [array]    $modsArray
    )

    $sumSize = 0
    $modsServer = import-server-modInfo
    foreach ($mod in $modsArray) {
        $modServer = ($modsServer | Where-Object {$_.'id' -eq $mod.id})
        $sumSize += ($modServer.real_size) -replace ',','.'
    }
    $sumSize = $sumSize
    return "{0:N2} MB" -f $sumSize
}
