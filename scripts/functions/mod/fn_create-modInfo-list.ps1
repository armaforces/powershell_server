function create-modInfo-list {
    param (
        [string]    $modsDir
    )

    $modsIDs = (Get-ChildItem -Path $modsDir) | Select-Object -ExpandProperty Name
    if ($modsIDs -contains "R3") {
        $modsIDs = $modsIDs | Where-Object {$_ -ne "R3"}
    }

    $modsInfo = SteamAPI-get-modInfo $modsIDs

    $mods = Create-List
    $specials = import-special-modInfo
    foreach ($modInfo in $modsInfo) {
        $serverSide = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_serverside -contains 'True'
        $map = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_map -contains 'True'
        $optional = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_optional -contains 'True'
        $mod = New-Object PSObject -Property @{
            id              = $modInfo.publishedfileid
            name            = $modInfo.title
            is_serverside   = $serverSide
            is_map          = $map
            is_optional     = $optional
            size            = $modInfo.file_size
            real_size       = get-modSize $modInfo.publishedfileid
            last_update     = get-DateFromEpoch $modInfo.time_updated
        }
        $mods += $mod
    }

    return $mods | Sort-Object -Property Name
}