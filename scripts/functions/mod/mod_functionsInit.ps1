#################################
#
#              MOD
#
#################################

# Function creates mod list from given mods
. "$PSScriptRoot\fn_build-modlist.ps1"

# Checks if any of supplied mods need updating
. "$PSScriptRoot\fn_check-mod-update.ps1"

# Function creates csv with info about all mods on server
. "$PSScriptRoot\fn_create-modInfo-list.ps1"

# Exports server mod info to modlist.csv
. "$PSScriptRoot\fn_export-server-modInfo.ps1"

# Returns real mod size on HDD in Bytes
. "$PSScriptRoot\fn_get-modSize.ps1"

# Function returns total mods size on HDD in MB
. "$PSScriptRoot\fn_get-modsSize.ps1"

# Returns mod's last update time based on meta.cpp
. "$PSScriptRoot\fn_Get-Mod-LastUpdate.ps1"

# Updates selected mods
. "$PSScriptRoot\fn_get-mod-update.ps1"

# Imports csv with special mods
. "$PSScriptRoot\fn_import-special-modInfo.ps1"

# Imports csv with server mods
. "$PSScriptRoot\fn_import-server-modInfo.ps1"

# Lists all available modset files
. "$PSScriptRoot\fn_List-Modsets.ps1"

# Returns all available modsets
. "$PSScriptRoot\fn_Get-ModsetsList.ps1"

# Handles modset selection by user
. "$PSScriptRoot\fn_Select-Modset.ps1"