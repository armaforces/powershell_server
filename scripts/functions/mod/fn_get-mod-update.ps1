function get-mod-update {
    param (
        [array]     $mods
    )
    
    Check-SteamCMD-ModUpdate $mods
}