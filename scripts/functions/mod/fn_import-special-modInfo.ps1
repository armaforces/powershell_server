function import-special-modInfo {
    
    try {
        $file = (Get-ChildItem -Path $modsetsDir | Where-Object {$_.Name -eq 'special.csv'}).FullName
        $specialMods = import-csv $file -Delimiter ';'
    } catch [System.Management.Automation.ItemNotFoundException] {
        Write-Host "special.csv not found."
        $specialMods = @()
    }

    return $specialMods
}