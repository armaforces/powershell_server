function import-server-modInfo {
    param (
        [string]    $source = 'modlist_cache'
    )

    $serverMods = Create-List

    $file = (Get-ChildItem -Path $modsetsDir | Where-Object {$_.Name -eq "$source.csv"})
    if ($null -eq $file) {
        Write-Host "No server modlist found!" -BackgroundColor Red
    } else {
        [array] $csvMods = import-csv $file.FullName -Delimiter ';'
        $serverMods.AddRange($csvMods)
        if ($null -eq $csvMods) {
            Write-Host "Server modlist is empty" -BackgroundColor Red
        }
    };

    return ,$serverMods
}
