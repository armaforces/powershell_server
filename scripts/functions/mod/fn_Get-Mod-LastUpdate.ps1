function Get-Mod-LastUpdate {
    param (
        $mod
    )
    try {
        return (Get-Item -Path "$modsDir\$($mod.id)\meta.cpp").LastWriteTime
    } catch {
        return (Get-Date)
    }
}