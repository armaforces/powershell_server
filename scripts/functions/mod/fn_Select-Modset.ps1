function Select-Modset {
    param (
        [string]    $defaultModset
    )
    
    $availableModsets = Get-ModsetsList
    List-Modsets $availableModsets
    $modsetInput = Get-Input "Please specify modset file name to run server [Enter to $defaultModset]: "
    if ($modsetInput -eq "") {
        return $defaultModset
    }
    $modset = ""
    try {
        $modsetInput = [int]$modsetInput
        $modset = $availableModsets[$modsetInput-1]
    } catch {
        try {
            $modset = ($availableModsets | Where-Object {$_.name -like "*$modsetInput*"})[0]
        } catch {
            Write-Host "No modset like $modsetInput found" -BackgroundColor Red
        }
    }

    if ($modset -eq "") {
        Write-Host "No modset selected, falling back to $defaultModset!"
        return $defaultModset
    }
    Write-Host "Selected $($modset.'Modset name')!"
    return $modset.'Modset name'
}