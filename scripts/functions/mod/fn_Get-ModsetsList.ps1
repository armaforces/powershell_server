function Get-ModsetsList {

    param (
        [int]    $sort = 0
    )

    $sortingTypes = @(
        'Modset name',
        'Mod quantity'
    )

    $availableModsets = Get-ChildItem -Path ("$modsetsDir\*") -Include *.csv
    $availableModsets | ForEach-Object {
        $_ | Add-Member 'Modset name' $_.Name.split('.')[0]
        $_ | Add-Member 'Mod quantity' ((Import-CSV $_.FullName).Count) 
    }
    $availableModsets | Sort-Object $sortingTypes[$sort]
    $i = 1
    $availableModsets | ForEach-Object {
        $_ | Add-Member 'No.' $i
        $i++
    }

    return $availableModsets
}