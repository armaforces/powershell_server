function export-server-modInfo {
    param (
        [array]     $mods,
        [string]    $destination = 'modlist_cache'
    )

    try {
        $mods | Export-Csv -Path "$modsetsDir\$destination.csv" -Delimiter ';'
    } catch [System.Management.Automation.ItemNotFoundException] {
        Write-Host "Could not export server mod info to $modsetsDir\$destination.csv"
    }
    Write-Host "Successfully exported server mod info to $modsetsDir\$destination.csv"
}
