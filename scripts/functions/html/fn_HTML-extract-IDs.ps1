function HTML-extract-IDs {
    param (
        [System.__ComObject]    $HTML
    )

    $links = $HTML.links | Select-Object -ExpandProperty innerHTML
    $IDs = Create-List
    foreach ($link in $links) {
        $id = $link.split('/')[-1].Substring(4)
        $IDs += $id
    }

    return $IDs
}