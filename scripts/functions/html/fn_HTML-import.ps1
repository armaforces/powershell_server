function HTML-import {
    param (
        [string]    $file
    )

    if (Test-Path -Path $file) {
        $source = Get-Content -Path $file -Raw
    } else {
        Write-Host "$file cannot be found." -BackgroundColor Red
        Write-Host "Terminating."
        Start-Sleep 3
        break
    }

    $HTML = New-Object -ComObject "HTMLFile"
    try {
        # This works in PowerShell with Office installed
        $HTML.IHTMLDocument2_write($source)
    }
    catch {
        # This works when Office is not installed    
        $source = [System.Text.Encoding]::Unicode.GetBytes($source)
        $HTML.write($source)
    }
    return $HTML
}