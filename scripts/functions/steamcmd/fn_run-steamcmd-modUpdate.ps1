function Run-SteamCMD-ModUpdate {
    param (
        [array] $modsID
    )

    if (-not (Test-Path "steam.txt")) {
        $SteamLogin = Read-Host "Steam Login: "
        $SteamPassword = Read-Host "Steam Password: " -AsSecureString
        "$steamLogin`ncache" | Out-File -FilePath "steam.txt"

    } else {
        Write-Host "Using cached credentials from steam.txt"
        $SteamDdd = Get-Content -Path "steam.txt"
        $SteamLogin = $SteamDdd[0]
        $SteamPassword = $SteamDdd[1]
    }

    $launchCommand = Get-SteamCMD-ModUpdateCommand $SteamPath $SteamLogin $SteamPassword $armaDir $modsID
    Start-Process $launchCommand[0] -ArgumentList ($launchCommand | Select-Object -Skip 1) -WindowStyle Minimized
    Write-Host "Waiting for steamcmd to finish."
    Wait-Process -Name 'steamcmd'
    Write-Host "Terminating."
}
