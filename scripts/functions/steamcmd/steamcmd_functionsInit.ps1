#################################
#
#           STEAMCMD
#
#################################

# Creates steamCMD startup command to download desired mods
. "$PSScriptRoot\fn_get-steamcmd-modUpdateCommand.ps1"

# Runs mod update based on array of mod IDs
. "$PSScriptRoot\fn_run-steamcmd-modUpdate.ps1"

# Checks if mods are correctly downloaded based on array of mod objects
. "$PSScriptRoot\fn_check-steamcmd-modUpdate.ps1"

# Updates/installs arma 3 server
. "$PSScriptRoot\fn_run-steamcmd-serverUpdate.ps1"
