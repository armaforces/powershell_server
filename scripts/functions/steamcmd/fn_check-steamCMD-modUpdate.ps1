function Check-SteamCMD-ModUpdate {
    param (
        [array] $mods,
        [bool]  $export = $true,
        [bool]  $force  = $false
    )

    $index = 0
    $serverMods = import-server-modInfo

    # Get Steam WS info about all mods that should be downloaded/updated
    $steamModsInfo = SteamAPI-get-modInfo ($mods | Select-Object -Skip $index -ExpandProperty 'id')

    foreach ($mod in $mods) {
        Write-Host ""
        $modID = $mod.'id'
        $modName = $mod.'name'
        
        $modObject = ($steamModsInfo | Where-Object {$_.publishedfileid -eq $mod.id})
        if ($modObject.GetType().FullName -eq "System.Object[]") {
            $modObject = $modObject.GetValue(0)
        }
        $modSize = $modObject.file_size / 1MB
        $modUpdatedWS = $modObject.time_updated
        Write-Host "Mod name: $modName"
        Write-Host "Mod ID: $modID"
        Write-Host "Mod size WS: $modSize"

        if (Test-Path "$modsDir\$modID") {

            $modRealSize = ((Get-ChildItem "$modsDir\$modID" -Recurse | Measure-Object -Property Length -Sum -ErrorAction SilentlyContinue).Sum / 1MB)
            $modLastUpdate = $mod.last_update_ws
            if ($null -eq $modLastUpdate) {
                $modLastUpdate = ($serverMods | Where-Object {$_.id -eq $modId}).last_update_ws
            }

            Write-Host "Mod size RL: $modRealSize"
            Write-Host "Mod update last: $modLastUpdate"
            Write-Host "Mod update WS: $modUpdatedWS"

            # Current size is lesser than Workshop size
            if ($modRealSize -lt $modSize) {
                Write-Host "$modName downloaded size is less than WS size. Resuming download."
                Run-SteamCMD-ModUpdate $mods[$index].id
                Write-Host "$modName finished downloading. Perfoming additional checks."
                Check-SteamCMD-ModUpdate $mod $false
            }
            # Workshop update date is newer than last time we downloaded the mod
            elseif (($null -eq $modLastUpdate) -or ($modLastUpdate -lt $modUpdatedWS)) {
                Write-Host "$modName has newer update date on workshop. Updating."
                Run-SteamCMD-ModUpdate $mods[$index].id
                # Update mod date after updating
                $mod | Add-Member last_update_ws $modUpdatedWS -Force

                Write-Host "$modName finished downloading. Perfoming additional checks."
                Check-SteamCMD-ModUpdate $mod $false
            }
            elseif ($force) {
                Write-Host "$modName. Forced update."
                Run-SteamCMD-ModUpdate $mods[$index].id
                # Update mod date after updating
                $mod | Add-Member last_update_ws $modUpdatedWS -Force

                Write-Host "$modName finished downloading. Perfoming additional checks."
                Check-SteamCMD-ModUpdate $mod $false
            }
            # All is fine
            else {
                Write-Host "$modName passed check successfully." -BackgroundColor Green
            }

        } else {
            Write-Host "$modName is not downloaded. Starting download of $modID."
            Run-SteamCMD-ModUpdate ($mods | Select-Object -Skip $index -ExpandProperty 'id')
            Write-Host "$modName finished downloading. Perfoming additional checks."
            Check-SteamCMD-ModUpdate $mod $false
        }

        # Always update server mods array
        $serverMod = $serverMods | Where-Object {$_.id -eq $modId}
        # Mod status is not cached, add to cache
        if ($null -eq $serverMod) {
            $serverMod = $mod.PsObject.Copy()
            $serverMod | Add-Member last_update (Get-Mod-LastUpdate $mod) -Force
            $serverMod | Add-Member real_size ($modRealSize) -Force

            $serverMods.Add($serverMod)
        } else {
            $serverMod.last_update = (Get-Mod-LastUpdate $mod)
            $serverMod.last_update_ws = $modUpdatedWS
            $serverMod.real_size = $modRealSize
        }

        $index += 1

        if ($export) {
            Export-Server-ModInfo $serverMods
        }
    }
}
