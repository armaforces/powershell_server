function Get-SteamCMD-ModUpdateCommand {
    param (
        [string]    $Steam,
        [string]    $SteamLogin,
        [string]    $SteamPassword,
        [string]    $armaDir,
        [array]     $modsID
    )

    foreach ($modID in $modsID) {
        $modsCommand += "+workshop_download_item 107410 $modID " 
    }
    if ($SteamPassword -ne 'cache') {
    return @(
        """$Steam""",
        "+@ShutdownOnFailedCommand 1",
        "+login $SteamLogin $SteamPassword",
#        "+force_install_dir ""$armaDir\mods""",
        $modsCommand,
        "+quit"
    )
    } else {
        return @(
            """$Steam""",
            "+@ShutdownOnFailedCommand 1",
            "+login $SteamLogin",
#            "+force_install_dir ""$armaDir\mods""",
            $modsCommand,
            "+quit"
        )
    }
}