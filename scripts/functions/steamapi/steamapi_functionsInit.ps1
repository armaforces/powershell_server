#################################
#
#           SteamAPI
#
#################################

# Creates SteamAPI request based on list of mod IDs
. "$PSScriptRoot\fn_steamAPI-modInfo-createRequest.ps1"

# Extract published file details for all mods from SteamAPI response
. "$PSScriptRoot\fn_steamAPI-modInfo-extractDetails.ps1"

# Handles all communication to return modInfo for given mod IDs
. "$PSScriptRoot\fn_steamAPI-get-modInfo.ps1"

# Converts modInfo or modIDs to mod objects with standard structure
. "$PSScriptRoot\fn_steamAPI-get-modObject.ps1"

# Makes SteamAPI request for mod info
. "$PSScriptRoot\fn_steamAPI-modInfo-makeRequest.ps1"