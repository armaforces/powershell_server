function steamAPI-get-modObject {
    param (
        [array]     $modsInput
    )

    # Check if input is IDs array or (else) modInfo array
    if ($modsInput[0].GetType() -eq ''.GetType() -or $modsInput.GetType() -eq (0).GetType()) {
        $modsInfo = SteamAPI-get-modInfo $modsInput
    } else {
        $modsInfo = $modsInput
    }
    
    $mods = Create-List
    $specials = import-special-modInfo
    foreach ($modInfo in $modsInfo) {
        $serverSide = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_serverside -contains 'True'
        $map = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_map -contains 'True'
        $optional = ($specials | Where-Object {$_.'name' -eq $modInfo.title} ).is_optional -contains 'True'
        $mod = New-Object PSObject -Property @{
            id              = $modInfo.publishedfileid
            name            = $modInfo.title
            is_serverside   = $serverSide
            is_map          = $map
            is_optional     = $optional
            size            = $modInfo.file_size
            real_size       = get-modSize $modInfo.publishedfileid
            last_update     = get-DateFromEpoch $modInfo.time_updated
            last_update_ws  = $modInfo.time_updated
        }
        $mods += $mod
    }

    return ,$mods
}