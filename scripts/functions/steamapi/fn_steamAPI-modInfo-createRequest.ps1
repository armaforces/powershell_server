function SteamAPI-modInfo-createRequest {
    param (
        [array]     $IDs
    )

    $postParams = @{itemcount=0}
    $countMods = 0
    foreach ($id in $IDs) {
        $postParams.Add("publishedfileids[$countMods]", $id)
        $countMods += 1
    };
    $postParams['itemcount'] = $countMods

    return $postParams
}