function SteamAPI-modInfo-makeRequest {
    param (
        $postParams
    )

    return Invoke-WebRequest -Uri https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1 -UseBasicParsing -Method POST -Body $postParams
}
