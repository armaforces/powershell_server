function SteamAPI-get-modInfo {
    param (
        [array]     $IDs
    )

    $postParams = SteamAPI-modInfo-createRequest $IDs
    $count = $postParams.count - 1
    Write-Host "Requesting info about $count mods from SteamAPI."
    try {
        $JSON = SteamAPI-modInfo-makeRequest $postParams
        Write-Host "Successful response from SteamAPI." -BackgroundColor Green
    } catch {
        Write-Host "SteamAPI failure." -BackgroundColor Red
    }
    $modsInfo = SteamAPI-modInfo-extractDetails $JSON

    return $modsInfo
}