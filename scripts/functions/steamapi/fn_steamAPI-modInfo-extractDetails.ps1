function SteamAPI-modInfo-extractDetails {
    param (
        $JSON
    )

    return ($JSON | Select-Object -ExpandProperty Content | ConvertFrom-JSON).response.publishedfiledetails
}