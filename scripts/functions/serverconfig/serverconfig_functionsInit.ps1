#################################
#
#         ServerConfig
#
#################################

# Handles server config loading
. "$PSScriptRoot\fn_Load-ServerConfig.ps1"

# Handles server config loading
. "$PSScriptRoot\fn_Load-ServerConfig-ModsetProfile.ps1"

# Returns common server config settings from common.json
. "$PSScriptRoot\fn_Get-ServerConfig-Common.ps1"

# Returns modset server config settings from config.json
. "$PSScriptRoot\fn_Get-ServerConfig-Modset.ps1"

# Returns empty server config cfg from assets directory
. "$PSScriptRoot\fn_Get-ServerConfig-EmptyServer.ps1"

# Returns empty basic config cfg from assets directory
. "$PSScriptRoot\fn_Get-ServerConfig-EmptyBasic.ps1"

# Changes corresponding key's value in cfg file
. "$PSScriptRoot\fn_ServerConfig-ChangeValue.ps1"

# Updates $configModsetDir $configKeysDir and $configProfileDir paths with desired modset
. "$PSScriptRoot\fn_Update-ModsetConfig-Path.ps1"