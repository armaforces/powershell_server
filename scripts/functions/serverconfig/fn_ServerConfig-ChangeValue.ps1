<#
FUNCTION:
    ServerConfig-ChangeValue

DESCRIPTION:
    Function handles replacing single key-value pair in raw config file
    Works for basic.cfg and server.cfg

PARAMS:
    [string]    raw config file to parse
    [string]    config key name
    [NONE]      config key value, might be a string, number or array

RETURN:
    [string]    raw config file with value for key replaced if possible
#>

function ServerConfig-ChangeValue {
    param (
        [string]    $config,
        [string]    $key,
        $value
    )

    # Build regex expression with new line before key to prevent mid-line replacements
    $expression = "`n$key"
    if ($key -eq 'template') {
        $expression = "`t`t$key"
    }
    # Check if $key contains '[]' eg. $key = 'admins[]' as then it's value should be an array
    if ($key -like '*[[\]]') {
        # Build regex for array value replacement
        # Magic so that $expression is equal to '$key\[\]'
        $expression = $key -replace "$($key -replace '[[\]]','')[[\]]","$($key -replace '[[\]]','')\[\"
    }
    $expression = "$expression = "+'.*;'

    # Check if there is such config entry
    $check = $config | Where-Object {$_ -match "$expression"}
    if ($check -eq $null) {
        # Return unchanged config
        return $config
    }
    try {
        # Get character after "$key = "
        # eg. for $key = 'lobbyTimeout' and config entry:
        # lobbyTimeout = 1234
        # returns 1
        # Used to determine if there are quotation marks needed
        $quote = ""
        if (($config.Substring($config.IndexOf("$key ="),$key.length+4)).Substring($key.length+3,1) -eq '"') {
            $quote = '"'
        }

        # Replace value
        if ($key -like '*[[\]]') {
            $config = ($config -replace ($expression),("$key = {$($value -join ',')};"))
        } else {
            $config = ($config -replace ($expression),("`n$key = $quote$value$quote;"))
        }

    } catch {
        Write-Host "$key = $value config replacement failed" -BackgroundColor Red
    }
    # Return changed config
    return $config

}