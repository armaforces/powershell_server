function Update-ModsetConfig-Path {
    param (
        [string]    $modset
    )

    $script:configModsetDir = $configModsetDir -f $modset
    $script:configKeysDir = $ExecutionContext.InvokeCommand.ExpandString($configKeysDir) -f $modset
    $script:configProfileDir = $ExecutionContext.InvokeCommand.ExpandString($configProfileDir) -f $modset
}