function Get-ServerConfig-Modset {
    param (
        [string] $modset
    )

    try {
        return (Get-Content -Path "$configModsetDir\config.json" | ConvertFrom-JSON)
    } catch {
        Throw "No modset config available!"
    }
}