function Get-ServerConfig-Common {
    
    try {
        return (Get-Content -Path $commonConfigPath | ConvertFrom-JSON)
    } catch {
        Throw "No common server config!"
    }
}
