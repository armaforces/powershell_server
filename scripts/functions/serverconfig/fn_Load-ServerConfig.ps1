function Load-ServerConfig {
    param (
        [string]    $modset
    )

    # Load empty configs
    $configServer = Get-ServerConfig-EmptyServer
    $configBasic = Get-ServerConfig-EmptyBasic

    # Load common settings
    try {
        $commonSettings = Get-ServerConfig-Common
    } catch {
        Write-Host "No common server config!" -BackgroundColor Red
    }

    # Load modset settings
    try {
        $modsetSettings = Get-ServerConfig-Modset $modset
    } catch {
        Write-Host "No modset config available!" -BackgroundColor Red
    }
    
    foreach ($configType in @('server','basic')) {
        $config = ""
        if ($configType -eq 'server') {
            $config = $configServer
        } else {
            $config = $configBasic
        }
        # Loop through all settings from modset and copy them to common settings to load afterwards
        try {
            if (($modsetSettings.PSObject.Properties | Select-Object -ExpandProperty Name) -notcontains $configType) {
                if ($configType -eq 'server') {
                    ($commonSettings | Select-Object -ExpandProperty $configType) | Add-Member -MemberType NoteProperty -Name 'hostName' -Value "Spesul Furses $modset Edition"
                }
                Throw "No modset $configType available."
            }
            if ($defaultModset -ne $modset -and (($modsetSettings | Select-Object -ExpandProperty $configType).PSObject.Properties | Select-Object -ExpandProperty Name) -notcontains 'hostName') {
                ($commonSettings | Select-Object -ExpandProperty $configType) | Add-Member -MemberType NoteProperty -Name 'hostName' -Value "Spesul Furses $modset Edition"
            }
            foreach ($property in (($modsetSettings | Select-Object -ExpandProperty $configType).PSObject.Properties | Select-Object -ExpandProperty Name)) {
                # Assign value to common settings
                ($commonSettings | Select-Object -ExpandProperty $configType) | Add-Member -MemberType NoteProperty -Name $property -Value (($modsetSettings | Select-Object -ExpandProperty $configType).$property) -Force
            }
        } catch {
            if ($PSItem.Exception.Message -ne "No modset $configType available.") {
                Write-Host "Error loading modset $configType config!" -BackgroundColor Red
            }
        }

        # Loop through all common settings (with modset settings included) and populate approperiate file
        foreach ($property in (($commonSettings | Select-Object -ExpandProperty $configType).PSObject.Properties | Select-Object -ExpandProperty Name)) {
            $config = ServerConfig-ChangeValue $config $property ($commonSettings | Select-Object -ExpandProperty $configType).$property
        }
        if ($configType -eq 'server') {
            $configServer = $config
        } else {
            $configBasic = $config
        }
    }

    try {
        $configBasic | Out-File -FilePath $configBasicPath -Force -Encoding utf8
        $configServer | Out-File -FilePath $configServerPath -Force -Encoding utf8
        Write-Host "Server configuration files successfully created!" -BackgroundColor Green
    } catch {
        Write-Host "Could not export server configuration files!" -BackgroundColor Red
    }

    try {
        Load-ServerConfig-ModsetProfile $modset
        Write-Host "Arma3Profile successfully loaded!" -BackgroundColor Green
    } catch {
        Write-Host "Could not load Arma3Profile!" -BackgroundColor Red
    }
}