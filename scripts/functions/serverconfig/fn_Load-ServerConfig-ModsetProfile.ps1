function Load-ServerConfig-ModsetProfile {
    param (
        [string]    $modset
    )

    $modsetProfileDir = "$configDir\modsetConfigs\$modset\profiles\server\Users\Server"
    $modsetProfilePath = "$modsetProfileDir\server.Arma3Profile"
    try {
        $modsetProfile = Get-Content -Path $modsetProfilePath
        if ($modsetProfile[0] -eq 'userOverride=1;') {
            return Write-Host "User profile override detected!"
        }
    } catch {
        Write-Host "No modset profile found! Loading common."
    }
    try {
        Copy-Item $commonProfilePath -Destination $modsetProfilePath -Force
    } catch {
        if (-not (Test-Path -Path $commonProfilePath)) {
            Write-Host "No common Arma3Profile found! Copying example file."
            Copy-Item $exampleProfilePath -Destination $commonProfilePath -Force
            Copy-Item $commonProfilePath -Destination $modsetProfilePath -Force
        }
    }
}