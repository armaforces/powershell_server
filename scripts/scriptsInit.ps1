
# Ensure that syntax errors will stop execution
$ErrorActionPreference = "Stop"

# Load script config
. "$PSScriptRoot\..\.config.ps1"
if (Test-Path "$PSScriptRoot\..\.config.local.ps1") {
    Write-Host "Local config override detected."
    . "$PSScriptRoot\..\.config.local.ps1"
}

# Load functions library
. "$PSScriptRoot\functionsInit.ps1"

