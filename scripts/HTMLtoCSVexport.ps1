Write-Host "Specify source file name." -BackgroundColor DarkBlue
$origin = Read-Host "HTML"

. "$PSScriptRoot\functions\HTML\HTML_functionsInit.ps1"
. "$PSScriptRoot\functions\misc\misc_functionsInit.ps1"
. "$PSScriptRoot\functions\mod\mod_functionsInit.ps1"
. "$PSScriptRoot\functions\steamapi\SteamAPI_functionsInit.ps1"

# HTML processing
Write-SectionSeparation "Processing HTML file"
$HTML = HTML-import "$origin.html"
Write-Host "Successfully imported $origin.html"
$IDs = HTML-extract-IDs $HTML
$count = $IDs.count
Write-Host "Extracted $count mod IDs."

# SteamAPI communication
$modsInfo = SteamAPI-get-modInfo $IDs

# Building mod list
Write-SectionSeparation "Building mod list"
$mods = (build-modlist $modsInfo) | Sort-Object -Property Name

Write-SectionSeparation "Exporting to *.csv file"
Write-Host "Specify destination *.csv file name in .\modsets\" -BackgroundColor DarkBlue
$destination = Read-Host "CSV"
$mods | Export-Csv -Path .\modsets\$destination.csv -Delimiter ';'
Write-Host "Successfully exported to $destination." -BackgroundColor Green
Write-Host "Exiting."
Start-Sleep 3