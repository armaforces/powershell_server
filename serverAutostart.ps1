$txt = ((Get-ChildItem -Path 'C:\Program Files (x86)\Steam\steamapps\common\Arma 3 Server\keys\*' -Include *.txt) | Select-Object -ExpandProperty Name)
# modset file format "!name.txt"
$modset = $txt.Substring(1,$txt.length-5)
$command = "$PSScriptRoot\Run-Server.ps1 $modset"
Invoke-Expression $command
