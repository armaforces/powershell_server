<#
#######################################################################
# SERVER STARTUP (don't even think about touching it)
#######################################################################
#>

param (
    [string]    $modset,
    [bool]    $headless = $false,
    [int]    $port = 2302
)

# Include functions library
. "$PSScriptRoot\scripts\scriptsInit.ps1"

Start-Logging -logsDir $logsDir -logFileName "Run-Server"

# If no modset param specified set last run modset as modset param
if ("" -eq $modset) {
    $modset = Get-Arma3-LastRun-Modset
}

# Let user change startup params from script until he is satisfied
Write-SectionSeparation "Server startup parameters selection"

Do {
    $serverType = if($headless -eq $false) {'Server'} else {'Headless Client'}
    $message = "$serverType will start with $modset modset on $port port. Do you wish to change something? [Y/any key]"
    # Ask user if he wants to change startup parameters, with timeout
    $changeParams = TimedPrompt $message 5
    if ($changeParams -eq 'Y') {
        # If user wishes to change params, call function handling param changes
        $newParams = Set-Arma3-StartupParams $modset $headless $port $serverType
        # Unpack new params from HashTable
        $modset = $newParams['modset']
        $headless = $newParams['headless']
        $port = $newParams['port']
    }
} until ($changeParams -ne 'Y')

# Update config path with selected modset
Update-ModsetConfig-Path $modset

# Run startup procedure
Run-Arma3-Server $modset $armaDir $port $headless

Stop-Logging
