param ([string]$modset)

# Include functions library
. "$PSScriptRoot\scripts\scriptsInit.ps1"

if ("" -eq $modset) {
    # Call function listing all available modsets before prompt
    List-Modsets
    $modset = Read-Host "`nPlease specify modset file name or Workshop ID to check and download mods`n or type ""server"" to update server files: "
}

if ("server" -eq $modset) {
    Run-SteamCMD-ServerUpdate
} else {
    # Update config path with selected modset
    Update-ModsetConfig-Path $modset

    try {
        $mods = Import-Csv -Path "$modsetsDir\$modset.csv" -Delimiter ';'
    } catch {
        Write-Host "No $modset.csv found. Assuming Workshop ID."
		$mods = Import-Csv -Path "$modsetsDir\modlist_cache.csv" -Delimiter ';'
		$mods = ($mods | Where-Object {$_.id -eq $modset})
    }

    Check-SteamCMD-ModUpdate ($mods | Where-Object {$_.id -ne "local"}) $true $true
};
