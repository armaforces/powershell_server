# Script lists all mods on server, sorting option available

. "$PSScriptRoot\scripts\scriptsInit.ps1"

$serverMods = import-server-modInfo

Write-Host "0 - Name"
Write-Host "1 - Last update"
Write-Host "2 - Size"
Write-Host "3 - ID"
$sortOptions = @(
    'name',
    'last_update',
    'real_size',
    'id'
)
$sort = $sortOptions[(Get-Input "Select sorting method: ")]

$serverMods | ForEach-Object {$_.real_size = ([math]::Round(($_.real_size -replace ',','.'),2))}
($serverMods |Sort-Object $sort | Format-Table -AutoSize -Property id, name, last_update, @{Label='Size (MB)'; Expression={$('{0:N2}' -f $_.real_size)}; Alignment='right'} | Out-String).Trim() | Write-Host

$leave = Get-Input "Enter anything to close window: "