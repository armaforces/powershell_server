powershell_server

Na pulpicie znajdują się skróty **(!)** do skryptów PowerShell służących do odpalania serwera, ich edycja jest zakazana, może doprowadzić do katastrofy. 

Skrypty są łatwe do zrozumienia i prowadzą użytkownika przez proces uruchamiania serwera, począwszy od ustalenia parametrów startowych, sprawdzenia czy mody są na miejscu, przez sprawdzenie ich aktualizacji oraz poprawności kluczy, aż do właściwego uruchomienia konsoli.

Serwer uruchamia się przechodząc przez następujące etapy:
- Ustalenie parametrów startowych serwera
- Załadowanie modsetu
- Sprawdzenie czy wszystkie mody z modsetu są dostępne na serwerze
- Sprawdzenie czy wszystkie mody z modsetu są zaktualizowane
- Sprawdzenie czy klucze modów nie zostały zaktualizowane
- Sprawdzenie czy wczytane zostały odpowiednie, aktualne klucze
- Wczytanie ustawień serwera (i ewentualne nadpisanie ustawieniami specjalnymi dla modsetu)
- Uruchomienie serwera
- Oczekiwanie na `Host Identity Created`
- Wyłączenie skryptu po 60 sekundach

By odpalić serwer nie musimy nic robić. Interesują nas tylko 3 podstawowe parametry - `$modset`, `$headless` i `$port`. Mody i configi są determinowane na podstawie parametru `$modset`, reszta mówi sama za siebie. Początkowe parametry to: `$modset` = ostatni modset, na którym był odpalony serwer; `$headless` = nie; `$port` = 2302. Wszystko to wyświetla się na początku skryptu uruchomieniowego wraz z zapytaniem czy użytkownik chce coś zmienić.

Następnie rozpocznie się właściwa procedura uruchamiania serwera podczas której możemy zostać zapytani o różne aktualizacje modów/kluczy a także w przypadku problemów (np. zostało wykryte, że serwer już działa).

**Uwaga!** Serwer zwykle włączy się pomimo nie wykonania aktualizacji modów/kluczy (pytania mają charaketer timeoutów, których zignorowanie powoduje przejście do kolejnej fazy uruchamiania). Może to spowodować niepoprawne jego działanie. Jeśli coś na serwerze nie działa jak należy to trzeba go wyłączyć i uruchomić skrypt od nowa uważnie obserwując proces uruchamiania w poszukiwaniu niebieskich (w ten sposób oznaczone są zapytania do użytkownika) oraz czerwonych (występujące błędy, zwykle nie kończące pracy skryptu) linii.

Dodanie nowego zbioru modów (np. Wietnam czy Postapo) sprowadza się na dobrą sprawę do przygotowania odpowiedniego pliku `*.csv` (np. `default.csv`) i umieszczenia go w odpowiednim folderze. Nie musimy edytować nic więcej.

W pliku **rozdzielanym przecinkami** zamieszczamy informacje o wszystkich modach w formacie:
- ID mod na Workshopie
- Nazwie moda
- Czy jest to mod serwerowy?
- Czy jest to mapa? (opcjonalne, jeśli chcemy umożlwić wczytanie HTML typu `NO_MAPS`)
- Czy jest to mod opcjonalny? (opcjonalne, jeśli chcemy zdefiniować własną listę modów opcjonalnych)

Te pliki można wygenerować na podstawie pliku HTML za pomocą narzędzia dostępnego [na stronie ArmaForces poświęconej modlistom](http://modlist.armaforces.com/#/tools/convert).

Wszystkie ustawienia znajdują się domyślnie w folderze `Arma 3 Server\serverConfig`

W głównym folderze ustawień serwerowych zawarte są foldery przeznaczone na ustawienia modsetowe (modsetConfigs) oraz na pliki `*.csv` modsetów (modsetModlists), a także globalne pliki konfiguracyjne: 
- `common.Arma3Profile` odpowiadający m.in. za ustawienia poziomu trudności
- `common.json` wykorzystywany do wypełniania `server.cfg` oraz `basic.cfg`
- `server.cfg` zawierający główne ustawienia serwera
- `basic.cfg` zawierający ustawienia serwera dotyczące połączenia

W modsetowych folderach ustawień (tj. `modsetConfigs\$modset`) znajdują się pliki:
- `server.Arma3Profile` nadpisujący domyślne ustawienia trudności (tylko pod warunkiem ustawienia `userOverride=1` w pierwszej linijce!)
- `config.json` nadpisujący ustawienia z `common.json`, ale tylko te, które zostały zdefiniowane w `config.json`, pozostałe będą pobrane z `common.json`

Domyślne pliki/wzorce wymienone wyżej znajdują się w folderze `assets` w katalogu skryptów.

Jeśli chcemy dodać jakieś ustawienia, które nie są jeszcze zdefiniowane w pliku `*.json` to po prostu dodajemy odpowiednie klucze i wartości, na przykład:
```
<wycinek empty_server.cfg>
motd[] = {""}; // Welcome message
motdInterval = 1;
headlessClients[] = {127.0.0.1};
```
To do pliku `*.json`, do słownika `server` dodajemy (oczywiście z zachowaniem standardu JSON):
```
        "motd[]": ["\"Witamy na moim serwerze\"","\"Bardzo się cieszę, że Was widzę\""],
        "motdInterval": 5,
        "headlessClients[]": ["127.0.0.1","127.0.0.2"]
```
I otrzymamy wtedy coś takiego w `server.cfg`:
```
<wycinek server.cfg>
motd[] = {"Witamy na moim serwerze","Bardzo się cieszę, że Was widzę"}; // Welcome message
motdInterval = 5;
headlessClients[] = {127.0.0.1, 127.0.0.2};
```
Trzeba tylko zwrócić uwagę na to jakiego rodzaju jest to wartość (jeśli to tablica to ma `[]` w nazwie), i jeśli to tablica to jak dokładnie jest zapisana (w tym przypadku `motd[]` zawiera `""`, więc trzeba wpisać `"\"<tutaj tekst>\""`). Oczywiście w przypadku configów modsetowych dajemy tylko te ustawienia, które chcemy nadpisać, więc przykładowy `config.json` modsetu może wyglądać tak:
```
{
    "server": {
        "hostName": "My fun server with default modset!"
    }
}
```
Wszystkie ustawienia zostaną wczytane najpierw z pliku `common.json`, a później z `config.json` z folderu modsetu (o ile będą dostępne) i zastąpią odpowiednie wpisy.