$HTML = New-Object -ComObject "HTMLFile"
$source = Get-Content -Path "AF_WYPAS_2019-04-07.html" -Raw
$HTML.IHTMLDocument2_write($source)

#$HTML.#links.length
#$HTML.ParsedHtml.getElementsByTagName('div')

$links = $HTML.links | Select-Object -ExpandProperty innerHTML
$IDs = @()
foreach ($link in $links) {
    $id = $link.split('/')[-1].Substring(4)
    $IDs = $IDs + $id
}

#$HTML.IHTMLDocument2_body
#$postParams = @{itemcount=1;'publishedfileids[0]'=463939057}
$postParams = @{itemcount=0}
#$xd = $postParams
#$xd.Add('publishedfileids[1]', 324242)

$countMods = 0
foreach ($id in $IDs) {
    $postParams.Add("publishedfileids[$countMods]", $id)
    $countMods += 1
};
$postParams['itemcount'] = $countMods


$d = Invoke-WebRequest -Uri https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1 -Method POST -Body $postParams
$dd = ($d | Select-Object -ExpandProperty Content | ConvertFrom-JSON).response.publishedfiledetails
$size = $dd.file_size
$timeUpdated = $dd.time_updated

# function get-DateFromEpoch ($epochDate) {
#     [timezone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($epochDate))
# }
# 
# get-DateFromEpoch $timeUpdated

$mods = @()
$specials = import-csv "modsets\special.csv" -Delimiter ';'
for ($i = 0; $i -lt $countMods; $i++) {
    $serverSide = ($specials | Where-Object {$_.'name' -eq $dd.title[$i]} ).is_serverside -contains 'true'
    $map = ($specials | Where-Object {$_.'name' -eq $dd.title[$i]} ).is_map -contains 'true'
    $optional = ($specials | Where-Object {$_.'name' -eq $dd.title[$i]} ).is_optional -contains 'true'
    $mod = New-Object PSObject -Property @{
        id              = $dd.publishedfileid[$i]
        name            = $dd.title[$i]
        is_serverside   = $serverSide
        is_map          = $map
        is_optional     = $optional
        size            = $dd.file_size[$i]
    }
    $mods += $mod
}

# Add optional mods
foreach ($mod in $specials) {
    $optional = $mod.is_optional -eq 'True'
    if ($optional -and -not $mods -contains $mod) {
        $serverSide = $mod.is_serverside -eq 'True'
        $map = $mod.is_map -eq 'True'
        $mod = New-Object PSObject -Property @{
            id              = $mod.id
            name            = $mod.name
            is_serverside   = $serverSide
            is_map          = $map
            is_optional     = $optional
            size            = $mod.file_size
        }
        $mods += $mod
    }
}



$mods | Format-table

$mods | Where-Object {$_.'is_serverside' -eq 'False'}

function Forum-Export {
    param (
        [array] $mods
    )

    $entry = "[$modName](http://steamcommunity.com/sharedfiles/filedetails/?id=$modID)"
}

$mods | Export-Csv -Path .\modsets\default.csv -Delimiter ';'