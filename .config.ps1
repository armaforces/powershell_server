# Default config values
#
# If you want to overwrite them copy this file to .config.local.ps1 change them in that file
#

#################################
#
#             SCRIPT
#
#################################

# Base directory for scripts
$baseDir = "$PSScriptRoot"
# Assets directory
$assetsDir = "$baseDir\assets"
# Logs directory
$logsDir = "$PSScriptRoot\Logs"

#################################
#
#             ARMA
#
#################################

# Arma bikey path
$armaBikey = "$assetsDir\a3.bikey"
# Arma server directory
$armaDir = "C:\Program Files (x86)\Steam\steamapps\common\Arma 3 Server"
# Arma server keys directory
$armaKeysDir = "$armaDir\keys"
# Arma mods directory
$modsDir = "$armaDir\mods"

#################################
#
#            CONFIG
#
#################################

# Arma server config directory. Contains modsets folders and .cfg .json global configs
$configDir = "$armaDir\serverConfig"
# Contains modset configs, {0} is used for $modset insertion after modset selection in Update-ModsetConfig-Path
$configModsetDir = "$configDir\modsetConfigs\{0}"
# Contains modset's keys folders
$configKeysDir = "$configModsetDir\keys"
# Contains server profile
$configProfileDir = "$configModsetDir\profiles"

#################################
#
#         SERVER CONFIG
#
#################################

# Default modset to run server with
$defaultModset = "default"
# Common basic & server config to load (copy, rename and fill example.json from assets)
$commonConfigPath = "$configDir\common.json"
# Contains server's connectivity config file
$configBasicPath = "$configDir\basic.cfg"
# Contains basic config file pattern to be filled by scripts and outputted to $configBasic
$configBasicBodyPath = "$assetsDir\empty_basic.cfg"
# Contains server config file loaded by server
$configServerPath = "$configDir\server.cfg"
# Contains server config file pattern to be filled by scripts and outputted to $configServer
$configServerBodyPath = "$assetsDir\empty_server.cfg"
# Default .Arma3Profile path
$commonProfilePath = "$configDir\common.Arma3Profile"
# Example .Arma3Profile path
$exampleProfilePath = "$assetsDir\example.Arma3Profile"
# serverPassword for headless client connection if $headless is true
$serverPassword = "tylkomirko4"

#################################
#
#             STEAM
#
#################################

# Steam CMD Exe path
$SteamPath = "F:\SteamCMD\SteamCMD.exe"

#################################
#
#           MODSETS
#
#################################

# Modsets directory (*.csv files)
$modsetsDir = "$configDir\modsetModlists"
